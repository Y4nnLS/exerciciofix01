
import java.util.Scanner;

public class Programa {

	 public static void main(String[] args) {
		
		 Scanner leitor = new Scanner(System.in);
		 
		 
		 String matricula;
		 System.out.println("Informe a matrícula:");
		 matricula = leitor.nextLine();
		 
		 String nome;
		 System.out.println("Informe o nome:");
		 nome = leitor.nextLine();
		 
		 int nota1;
		 System.out.println("Informe a primeira nota: ");
		 nota1 = leitor.nextInt();
		 
		 int nota2;
		 System.out.println("Informe a segunda nota: ");
		 nota2 = leitor.nextInt();
		 
		 int notaFinal = (nota1 + nota2) / 2;
		 
		 System.out.println("Matrícula: " + matricula);
		 System.out.println("Nome: " + nome);
		 if (notaFinal >= 6) {
			 System.out.println("Aprovado!");
		 } else {
			 System.out.println("Reprovado!");
		 }
		 
		 System.out.println("Nota final: " + notaFinal);
		 
		 leitor.close();
		 
	}
	
	
}
